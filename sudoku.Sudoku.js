
// Sudoku object
// foo bar

function Sudoku() {
	this.cells = new Array();

	// add a cell
	this.addCell = function( index, cell ) {
		this.cells[index] = cell;
	}


	this.solved = function() {
		solved = true;

		for (var i = 0; i<81;i++) {
			solved = solved && this.cells[i].solved;
		}

		return solved;
	}

	this.findAndPaint  = function() {
		for (var i=0;i<81;i++) {
			this.cells[i].findAllowedValues(this);
		}

		this.paint();
	}

	// paint the current status
	this.paint = function() {
		for ( var i=0; i<this.cells.length; i++ ) {
			this.cells[i].display();
		}
	}

	// mark all occurences of the given digit
	this.mark = function(digit) {
		var markCounter = 0;
		for ( var i=0; i<this.cells.length; i++ ) {
			//console.log("i ["+i+"]");
			markCounter += this.cells[i].mark(digit);
		}
		return markCounter;
	}

	// try solving a given cell by checking all digits
	// whether they are the only possible solution for this cell
	this.bruteForceCell = function( index ) {
		var currentCell = this.cells[ index ];
		currentCell.findAllowedValues(this);
		//console.log(currentCell);
		// var square = this.squares[ currentCell.square ];
		var av = currentCell.allowedValues;

		if ( av.length == 1 ) {
				this.cells[ index ].solved = true;
				this.cells[ index ].value = av[0];
				return true;
		}
		for ( var x=0; x < av.length; x++) {
			if ( !this.findInSquare(index, av[x], false) || !this.findInRow(index, av[x], false) || !this.findInCol(index, av[x], false) ) {
				this.cells[ index ].solved = true;
				this.cells[ index ].found = 1;
				this.cells[ index ].value = av[x];
				return true;
			}
		}
		return false;
	}

	// get 9 cells from a 3x3 square
	this.slice = function ( square ) {
		var squareArray = new Array();
		for ( var i=0; i<81;i++) {
			if ( this.cells[i].square == square ) {
				squareArray[squareArray.length] = this.cells[i];
			}
		}
		return squareArray;
	}

	this.findInSquare = function( index, value, solved ) {
		var found = false;
		var startIndex = Math.floor( index / 9 ) * 9;

		var currentSquare = this.cells[index].square;
		var squareArray = this.slice(currentSquare);

		for ( var i = 0; i < 9; i++ ) {
			var c = squareArray[i];
			if ( c.index != index ) {
				// check only for already solved cells
				if ( solved ) {
					if ( c.solved ) {
						if ( c.value == value ) {
							return true;
						}
					}
				} else {
					if ( !c.solved ) {
						if ( c.allowedValues.indexOf(value) >= 0 ) {
							return true;
						}
					}
				}
			}
		}
		return found;
	}

	this.findInRow = function( index, value, solved ) {
		var found = false;
		var startIndex = Math.floor( index / 9 ) * 9;

		for ( var i = startIndex; i < startIndex + 9; i++ ) {
			var c = this.cells[i];
			if ( i != index ) {
				// check only for already solved cells
				if ( solved ) {
					if ( c.solved ) {
						if ( c.value == value ) {
							return true;
						}
					}
				} else {
					if ( !c.solved ) {
						if ( c.allowedValues.indexOf(value) >= 0 ) {
							return true;
						}
					}
				}
			}
		}
		return found;
	}

	this.findInCol = function( index, value, solved ) {
		var found = false;
		var startIndex = index % 9;

		for ( var i = startIndex; i < startIndex + 9*9; i +=9 ) {
			var c = this.cells[i];
			if ( i != index ) {
				// check only for already solved cells
				if ( solved ) {
					if ( c.solved ) {
						if ( c.value == value ) {
							return true;
						}
					}
				} else {
					if ( !c.solved ) {
						if ( c.allowedValues.indexOf(value) >= 0 ) {
							return true;
						}
					}
				}
			}
		}
		return found;
	}

}
