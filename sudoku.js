// try to solve a sudoku by brute force / try and error
//

emptyCells = 0;


// initialize data structures: these are the 3x3 square squares starting with
// index 0 at the top left
squares = [
	[6,0,0,3,2,0,0,0,0],
	[5,0,1,0,0,0,0,4,0],
	[0,0,0,4,0,0,0,2,0],
	[7,1,0,5,0,0,0,0,0],
	[6,5,0,0,0,0,0,8,0],
	[2,3,0,0,0,1,0,0,6],
	[1,0,0,0,0,6,0,7,3],
	[7,0,2,0,0,9,0,0,0],
	[6,5,4,0,8,0,0,0,0]
]

$(document).ready( function(){

	var counter = 1;
	var puzzle = document.getElementById("puzzle");
	var html = puzzle.innerHTML;


	var sudoku = new Sudoku();

	// draw the grid and populate the row/column arrays
	for ( var squareIndex = 0; squareIndex<9; squareIndex++) {
		html = html + '<div id="square'+squareIndex+'" class="outerCell">';

		for ( var cellIndex = 0; cellIndex<9; cellIndex++) {
			var row = Math.floor(cellIndex / 3)  + Math.floor( squareIndex / 3 ) * 3;
			var col = (cellIndex % 3) + ( squareIndex % 3 ) * 3;
			var content = squares[squareIndex][cellIndex];
			var index = row*9+col;

			sudoku.addCell(index, new Cell( index, squareIndex, row, col, content));

			html = html + '<div id="'+index+'" class="innerCell"></div>';
		}
		html = html + '</div>';
	}

	puzzle.innerHTML = html;

	sudoku.paint();


	$(".innerCell").click( function() {
		var markCounter = 0;
		var index = $(this).attr('id');
		//console.log(index);
		var solved = sudoku.cells[index].solved;
		if ( solved ) {
			var markDigit = sudoku.cells[index].value;
			markCounter = sudoku.mark(markDigit);
		}
		console.log("mark ["+markDigit+"] counter [" + markCounter + "]");
	});

	$("#checkFull").click( function() {
		sudoku.paint();
	});

	$("#checkDebug").click( function() {
		sudoku.paint();
	});

	$("#markDigit").click( function() {
		var markDigit = Number($('#markDigit').val());
		console.log("mark ["+markDigit+"]");
		sudoku.mark(markDigit);
	});

	$("#markDigit").change( function() {
		var markDigit = Number($('#markDigit').val());
		console.log("mark ["+markDigit+"]");
		sudoku.mark(markDigit);
	});

	$("#buttonMark").click( function() {
		var markDigit = Number($('#markDigit').val());
		console.log("mark ["+markDigit+"]");
		sudoku.mark(markDigit);
	});


	$("#buttonPaint").click( function() {
		var found = true;
		while (found) {
			found = false;
			for ( var i=0; i<81;i++) {
				if ( sudoku.cells[i].findAllowedValues(sudoku)) {
					found = found || true;
					console.log("i ["+i+"] found ["+found+"]")
				}
			}
		}
		console.log("i ["+i+"] found ["+found+"]")
		sudoku.paint();
		if (sudoku.solved()) {
			$("#signalSolved").removeClass("redButton").addClass("greenButton");
			$("#puzzle").removeClass("redButton").addClass("greenButton");
		} else {
			$("#signalSolved").removeClass("greeButton").addClass("redButton");
			$("#puzzle").removeClass("greeButton").addClass("redButton");
		}
	});

	round=0;
	$("#buttonSolve").click( function() {
		if (sudoku.solved()) {
			console.log(Date());
		} else {
			round++;
			var found = true;
			while (found) {
				found = false;
				for ( var i=0; i<81;i++) {
					if ( sudoku.cells[i].findAllowedValues(sudoku)) {
						found = found || true;
						console.log("i ["+i+"] found ["+found+"]")
					}
				}
			}
			console.log("i ["+i+"] found ["+found+"]")
			sudoku.paint();

			for ( var i=0; i<81;i++) {
				if ( sudoku.bruteForceCell(i) ) {
					console.log("Solved cell ["+i+"].");
					break;
				}
			}
			sudoku.paint();
		}
		if (sudoku.solved()) {
			$("#signalSolved").removeClass("redButton").addClass("greenButton");
			$("#puzzle").removeClass("redButton").addClass("greenButton");
		} else {
			$("#signalSolved").removeClass("greeButton").addClass("redButton");
			$("#puzzle").removeClass("greeButton").addClass("redButton");
		}
		console.log("Round ["+round+"] done.");

	});
});
