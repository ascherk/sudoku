// try to solve a sudoku by brute force / try and error
//

function Cell( index, square, row, col, value ) {

	this.square = square;
	this.row = row;
	this.col = col;
	this.index = index;

	this.allowedValues = new Array();

	this.solved = false;
	this.found = 0;
	this.value = value;
	this.divId = '#' + index;

	if ( this.value > 0 ) {
		this.solved = true;
	} else {
		this.solved = false;
	}


	// check square, row and col for values, that are not present there. These are potential values of this cell.
	this.findAllowedValues = function(sudoku) {
		//console.log("findAllowedValues: index"+this.index);
		this.allowedValues = new Array();
		if ( !this.solved ) {
			// check for each digit 1-9 if it is already contained as (solved) value in row, col or square
			// add to allowedValues, if not
			for ( var search = 1; search <= 9; search++ ) {
				if ( !sudoku.findInSquare( this.index, search, true ) && !sudoku.findInRow( this.index , search, true ) && !sudoku.findInCol( this.index , search, true ) ) {
					this.allowedValues[this.allowedValues.length] = search;
					//console.log("findAllowedValues: add"+search);
				}
			}
			// only one allowed values found means, this is the cell value
			if (this.allowedValues.length == 1 ) {
				this.solved = true;
				this.found = 2;
				this.value = this.allowedValues[0];
				console.log("findAllowedValues: found: ["+this.index+"]["+this.value+"]");
				return true;
			} else {
				//console.log("findAllowedValues: posibilities found: ["+this.index+"]["+this.allowedValues.length+"]");
			}
		} else {
			//console.log("findAllowedValues: alread solved: ["+this.index+"]["+this.value+"]");
		}
		//console.log("fav: idx ["+this.index + "] found ["+this.found+"]");
		return false;
	}

	// build HTML for one cell
	this.display = function () {
		//console.log("full display ["+$('#checkFull').is(":checked")+"]")
		//console.log("debug display ["+$('#checkDebug').is(":checked")+"]")

		$(this.divId).removeClass("marked", 0);

		if ( $('#checkDebug').is(":checked") ) {
			var html = '<p class="debug">i:'+this.index + '/b:' + this.square+'/r:'+this.row+'/c:'+this.col+'</p>' + '<p class="content">';
		} else {
			var html = '<p class="content">';
		}

		if ( !this.solved ) {
			if ( $('#checkFull').is(":checked") ) {
				html = html +this.allowedValues.join('/');
			}
		} else {
			html = html +this.value;
		}
		html = html + '</p>';
		$(this.divId).html(html);


		if ( this.solved ) {
			$(this.divId).removeClass("redInnerCell", 50);
			if ( this.found == 2 ) {
				$(this.divId).addClass("dropedInnerCell", 50);
			} else {
				if (this.found == 1 ) {
					$(this.divId).addClass("solvedInnerCell", 50);
				} else {
					$(this.divId).addClass("yellowInnerCell", 50);
				}
			}
		} else {
			$(this.divId).addClass("redInnerCell", 50);
		}
	}

	// highlight a cell if it is solved and the solved value
	// equals digit
	//
	this.mark = function (digit) {
		this.display();

		if ( this.value === digit ) {
			$(this.divId).removeClass("redInnerCell", 90);
			$(this.divId).removeClass("dropedInnerCell", 90);
			$(this.divId).removeClass("solvedInnerCell", 90);
			$(this.divId).removeClass("yellowInnerCell", 90);
			//$(this.divId).effect("shake");
			$(this.divId).addClass("marked", 700, "easeInQuad");
			//console.log("["+this.value+"]-["+this.index+"]");
			return 1;
		} else {
			return 0;
		};

	}

}
